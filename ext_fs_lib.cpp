#include "ext_fs_lib.h"

#include <cstdlib>

#if __has_include( <windows.h>) && (defined(_WIN32) || defined(_WIN64))
#	define USE_WINAPI
#endif

#ifdef USE_WINAPI
#	include <windows.h>
#	include <shlobj.h>
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
namespace {
    fs::path get_dir( int dir_id ) {
        wchar_t system_path[MAX_PATH];
        if ( SHGetFolderPathW( NULL, dir_id, NULL, SHGFP_TYPE_CURRENT, system_path ) == S_OK ) return system_path;
        return fs::path();
    }
} // namespace
#else
#	include <cerrno>
#	include <dlfcn.h>
#	include <stdexcept>
#	include <string>
#	include <string_view>
[[maybe_unused]] __attribute__( ( visibility( "default" ) ) ) void *ext_fs_lib__ImageBase;

namespace {
    fs::path find_program( std::string_view name ) {
        std::string_view path = std::getenv( "PATH" );
        while ( true ) {
            auto path_len = path.find( ':' );

            auto exe_path = fs::path( path.substr( 0, path_len ) ) / name;
            if ( fs::exists( exe_path ) ) return exe_path;

            if ( path_len == std::string_view::npos ) break;
            path = path.substr( path_len + 1 );
        }
        return name;
    }
    std::string exec_program( const std::string &cmdline ) {
        static constexpr auto read_buffser_size = 4096;

        auto fp = popen( cmdline.c_str(), "r" );
        if ( fp == nullptr ) {
            if ( errno == EINVAL ) throw std::invalid_argument( "Can't popen program with mode \"r\"" );
            throw std::runtime_error( "Can't launch program" );
        }

        std::string result;

        char output[read_buffser_size];
        while ( fgets( output, sizeof( output ), fp ) != nullptr ) result += output;

        auto status = pclose( fp );
        if ( status == -1 ) throw std::logic_error( "Can't normaly close program" );

        if ( status != 0 ) throw std::runtime_error( result );

        return result;
    }
    fs::path get_xdg_path( std::string_view dir = "" /*HOME*/ ) {
        auto xdg_user_dir = find_program( "xdg-user-dir" );
        if ( !xdg_user_dir.empty() ) {
            try {
                auto path = exec_program( xdg_user_dir.string() + " " + std::string( dir ) );
                while ( path.back() == '\r' || path.back() == '\n' ) path.pop_back();
                return path;
            } catch ( std::invalid_argument &e ) {
            } catch ( std::logic_error &e ) {
            } catch ( std::runtime_error &e ) {}
        }
        return fs::path();
    }
} // namespace
#endif

fs::path fs::exe_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        wchar_t system_path[MAX_PATH];
        GetModuleFileNameW( GetModuleHandle( nullptr ), system_path, MAX_PATH );
        cached_path = system_path;
#else
        cached_path = read_symlink( "/proc/self/exe" );
#endif
    }
    return cached_path;
}

fs::path fs::self_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        wchar_t system_path[MAX_PATH];
        GetModuleFileNameW( (HMODULE)&__ImageBase, system_path, MAX_PATH );
        cached_path = system_path;
#else
        Dl_info info;
        dladdr( (void *)&ext_fs_lib__ImageBase, &info );
        cached_path = info.dli_fname;
#endif
    }
    return cached_path;
}

fs::path fs::home_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        cached_path = get_dir( CSIDL_PROFILE );
#else
        auto home_path = std::getenv( "HOME" );
        if ( home_path != nullptr && *home_path != '\0' )
            cached_path = home_path;
        else
            cached_path = get_xdg_path();
#endif
    }
    return cached_path;
}

fs::path fs::configs_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        cached_path = get_dir( CSIDL_APPDATA );
        if ( cached_path.empty() ) cached_path = std::getenv( "APPDATA" );
#else
        auto config_path = std::getenv( "XDG_CONFIG_HOME" );
        if ( config_path != nullptr && *config_path != '\0' )
            cached_path = config_path;
        else
            cached_path = home_path() / ".config";
#endif
    }
    return cached_path;
}

fs::path fs::data_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        cached_path = get_dir( CSIDL_LOCAL_APPDATA );
#else
        auto data_path = std::getenv( "XDG_DATA_HOME" );
        if ( data_path != nullptr && *data_path != '\0' )
            cached_path = data_path;
        else
            cached_path = home_path() / ".local" / "share";
#endif
    }
    return cached_path;
}

fs::path fs::cache_path() {
    static path cached_path;
    if ( cached_path.empty() ) {
#ifdef USE_WINAPI
        cached_path = get_dir( CSIDL_INTERNET_CACHE );
#else
        auto data_path = std::getenv( "XDG_CACHE_HOME" );
        if ( data_path != nullptr && *data_path != '\0' )
            cached_path = data_path;
        else
            cached_path = home_path() / ".cache";
#endif
    }
    return cached_path;
}

fs::path fs::documents_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_PERSONAL );
#else
    static path cached_path = get_xdg_path( "DOCUMENTS" );
#endif
    return cached_path;
}

fs::path fs::desktop_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_DESKTOPDIRECTORY );
#else
    static path cached_path = get_xdg_path( "DESKTOP" );
#endif
    return cached_path;
}

fs::path fs::music_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_MYMUSIC );
#else
    static path cached_path = get_xdg_path( "MUSIC" );
#endif
    return cached_path;
}

fs::path fs::videos_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_MYVIDEO );
#else
    static path cached_path = get_xdg_path( "VIDEOS" );
#endif
    return cached_path;
}

fs::path fs::pictures_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_MYPICTURES );
#else
    static path cached_path = get_xdg_path( "PICTURES" );
#endif
    return cached_path;
}

fs::path fs::templates_path() {
#ifdef USE_WINAPI
    static path cached_path = get_dir( CSIDL_TEMPLATES );
#else
    static path cached_path = get_xdg_path( "TEMPLATES" );
#endif
    return cached_path;
}


#ifdef USE_WINAPI
#	undef USE_WINAPI
#endif
