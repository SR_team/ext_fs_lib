#pragma once

#include <filesystem>

namespace fs {
    using namespace std::filesystem;

    /// Path to executable file
    path exe_path();
    /// Path to self binary (exe/lib)
    path self_path();
    /// User personal directory
    path home_path();
    /// User configs (APPDATA) directory
    path configs_path();
    /// User data (LOCAL APPDATA) directory
    path data_path();
    /// User cache directory
    path cache_path();
    /// User documents directory
    path documents_path();
    /// User desktop directory
    path desktop_path();
    /// User music directory
    path music_path();
    /// User videos directory
    path videos_path();
    /// User pictures directory
    path pictures_path();
    /// User templates directory
    path templates_path();
} // namespace fs
